# Webflux R2dbc


**기술스펙**
* JDK 11
* Kotlin
  * `Coroutine`
* Spring
  * `spring-boot-starter-webflux` + `kotlinx-coroutines-reactor`
  * `spring-boot-starter-data-r2dbc` + `name.nkonev.r2dbc-migrate`  

**로컬 환경 세팅**
* locale database
```
docker run -d -p 5432:5432 -e POSTGRES_PASSWORD=password --name postgres postgres

-- postgresql 접속 후 데이터베이스 생성 쿼리 실행
create database dummy;
```

**패키지 구성**
* controller, router : 엔트포인트를 처리한다.
* config : 프로젝트에 대한 설정을 가진다.
* model : 요청/응답 및 내부적으로 필요한 데이터성 클래스 정보를 가진다.
* entity : 데이터베이스과 관련된 데이터성 클래스 정보(엔티티)를 가진다.
* component : 특정 비즈니스 로직과는 관계없지만, 공통적으로 처리되어야 하는 로직을 담는다.
* service : 비즈니스 로직을 처리한다.
* utils : 특정 레이어에 종속성을 가지지 않고 함수적인 기능을 하는 유틸리티성 처리를 담는다. 
