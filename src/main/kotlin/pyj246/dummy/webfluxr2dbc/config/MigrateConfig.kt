package pyj246.dummy.webfluxr2dbc.config

import io.r2dbc.pool.PoolingConnectionFactoryProvider.INITIAL_SIZE
import io.r2dbc.pool.PoolingConnectionFactoryProvider.MAX_IDLE_TIME
import io.r2dbc.pool.PoolingConnectionFactoryProvider.MAX_LIFE_TIME
import io.r2dbc.pool.PoolingConnectionFactoryProvider.MAX_SIZE
import io.r2dbc.spi.ConnectionFactories
import io.r2dbc.spi.ConnectionFactoryOptions.DATABASE
import io.r2dbc.spi.ConnectionFactoryOptions.DRIVER
import io.r2dbc.spi.ConnectionFactoryOptions.HOST
import io.r2dbc.spi.ConnectionFactoryOptions.PASSWORD
import io.r2dbc.spi.ConnectionFactoryOptions.PORT
import io.r2dbc.spi.ConnectionFactoryOptions.USER
import io.r2dbc.spi.ConnectionFactoryOptions.builder
import java.io.File
import java.io.InputStream
import java.nio.charset.Charset
import java.time.Duration
import name.nkonev.r2dbc.migrate.core.MariadbQueries
import name.nkonev.r2dbc.migrate.core.PostgreSqlQueries
import name.nkonev.r2dbc.migrate.core.R2dbcMigrate
import name.nkonev.r2dbc.migrate.core.R2dbcMigrateProperties
import name.nkonev.r2dbc.migrate.reader.MigrateResource
import name.nkonev.r2dbc.migrate.reader.MigrateResourceReader
import org.reflections.Reflections
import org.reflections.scanners.ResourcesScanner
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.EventListener
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order

/**
 * 마이그레이션 설정 클래스.
 */
@Configuration
class MigrateConfig(
    @Value("\${datasource.dummy.migrate.protocol}") val protocol: String,
    @Value("\${datasource.dummy.migrate.host}") val host: String,
    @Value("\${datasource.dummy.migrate.port}") val port: Int,
    @Value("\${datasource.dummy.migrate.database}") val database: String,
    @Value("\${datasource.dummy.migrate.username}") val username: String,
    @Value("\${datasource.dummy.migrate.password}") val password: String,
    @Value("\${datasource.dummy.migrate.resources-paths}") val resourcesPaths: List<String>,
    @Value("\${datasource.dummy.migrate.table}") val table: String
) {
    @Order(Ordered.HIGHEST_PRECEDENCE)
    @EventListener(ApplicationReadyEvent::class)
    fun migrate() {
        val properties = R2dbcMigrateProperties()
        properties.resourcesPaths = resourcesPaths

        // make compression files
        val fileReader = MigrateResourceCompressorReader(properties.fileCharset)

        // migrate
        R2dbcMigrate.migrate(
            connectionFactory(), properties, fileReader, PostgreSqlQueries(null, table, "${table}_lock")
        ).block()

        // delete compression files
        fileReader.deleteAll()
    }

//    fun connectionFactory()3 = MariadbConnectionFactory(
//        MariadbConnectionConfiguration.builder()
//            .host(host)
//            .port(port)
//            .database(database)
//            .username(username)
//            .password(password)
//            .build()
//    )

    fun connectionFactory() = ConnectionFactories.get(
        builder()
            .option(DRIVER, protocol)
            .option(HOST, host)
            .option(PORT, port)
            .option(DATABASE, database)
            .option(USER, username)
            .option(PASSWORD, password)
            .option(INITIAL_SIZE, 1)
            .option(MAX_SIZE, 1)
            .option(MAX_IDLE_TIME, Duration.ofSeconds(60))
            .option(MAX_LIFE_TIME, Duration.ofSeconds(60))
            .build()
    )
}

/**
 * 마이그레이션 대상 파일 리더 클래스.
 * r2dbc-migrate 라이브러리가 읽을 수 있도록 대상 파일을 압축하여 전달한다.
 */
class MigrateResourceCompressorReader(
    private val fileCharset: Charset,
    private val compressionRootPath: String = ".comp_migrate"
) : MigrateResourceReader {

    private val fileList = ArrayList<File>()

    override fun getResources(locationPattern: String?): MutableList<MigrateResource> {
        val reflections = Reflections(locationPattern, ResourcesScanner())
        val resources = reflections.getResources { s: String -> s.endsWith(".sql") }
        return resources.map { path ->
            val rootPath = if (compressionRootPath.endsWith("/")) {
                compressionRootPath.substring(0, compressionRootPath.length - 1)
            } else {
                compressionRootPath
            }
            val compressionPath = "$rootPath/${path.substring(0, path.length - 4)}__split.sql"

            val compressionFile = File(compressionPath)
            compressionFile.deleteOnExit()
            compressionFile.parentFile.mkdirs()
            compressionFile.createNewFile()
            compressionFile.bufferedWriter(fileCharset).use { bufferedReader ->
                val stream = javaClass.getResourceAsStream("/$path")
                if (stream != null) {
                    var queryLineList: ArrayList<String> = ArrayList()
                    stream.bufferedReader(fileCharset).readLines().forEach { lineString ->
                        if (lineString.trim().isNotBlank()) {
                            if (!lineString.trimStart().startsWith("--")) {
                                queryLineList.add(lineString)
                                if (lineString.trimEnd().endsWith(";")) {
                                    bufferedReader.write(queryLineList.joinToString(" "))
                                    bufferedReader.newLine()
                                    queryLineList = ArrayList()
                                }
                            }
                        }
                    }
                    if (queryLineList.isNotEmpty()) {
                        throw RuntimeException("migrate file compress error.(reason: file empty, file: $path)")
                    }
                } else {
                    throw RuntimeException("migrate file compress error.(reason: stream is null, file: $path)")
                }
            }
            fileList.add(compressionFile)
            CompressionMigrateResource(compressionFile)
        }.toMutableList()
    }

    /**
     * 파일 삭제.
     */
    fun deleteAll() {
        fileList.forEach {
            if (it.exists()) {
                it.delete()
            }
        }
    }

    /**
     * 마이그레이션을 위한 압축된 리소스 클래스.
     */
    class CompressionMigrateResource(
        private val file: File
    ) : MigrateResource {
        override fun isReadable() = true
        override fun getInputStream(): InputStream = file.inputStream()
        override fun getFilename(): String = file.name
    }
}
