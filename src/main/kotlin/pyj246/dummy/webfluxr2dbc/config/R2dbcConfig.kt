package pyj246.dummy.webfluxr2dbc.config

import io.r2dbc.spi.ConnectionFactory
import kotlinx.coroutines.reactor.ReactorContext
import kotlinx.coroutines.reactor.mono
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration
import org.springframework.data.r2dbc.convert.R2dbcConverter
import org.springframework.data.r2dbc.core.DefaultReactiveDataAccessStrategy
import org.springframework.data.r2dbc.core.R2dbcEntityOperations
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate
import org.springframework.data.r2dbc.dialect.MySqlDialect
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.r2dbc.connection.R2dbcTransactionManager
import org.springframework.r2dbc.connection.lookup.AbstractRoutingConnectionFactory
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.transaction.reactive.TransactionalOperator
import pyj246.dummy.webfluxr2dbc.config.property.R2dbcProperties
import pyj246.dummy.webfluxr2dbc.model.r2dbc.ConnectionType
import pyj246.dummy.webfluxr2dbc.model.r2dbc.ConnectionType.READ
import pyj246.dummy.webfluxr2dbc.model.r2dbc.ConnectionType.WRITE
import pyj246.dummy.webfluxr2dbc.model.r2dbc.R2dbcConnectionType
import pyj246.dummy.webfluxr2dbc.model.r2dbc.R2dbcConnectionType.Companion.REACTOR_CONTEXT_CONNECTION_TYPE
import reactor.core.publisher.Mono

// Bean Names
const val DUMMY_CONNECTION_FACTORY = "dummyConnectionFactory"
const val DUMMY_TRANSACTION_MANAGER = "dummyTransactionManager"
const val DUMMY_TRANSACTIONAL_OPERATOR = "dummyTransactionalOperator"
const val DUMMY_ENTITY_OPERATIONS = "dummyEntityOperations"

/**
 * R2dbc 설정 클래스.
 */
@EnableR2dbcRepositories(
    entityOperationsRef = DUMMY_ENTITY_OPERATIONS,
    basePackages = ["pyj246.dummy.webfluxr2dbc.repository.dummy"]
)
@Configuration
class R2dbcConfig(
    private val writeR2dbcProperties: DummyWriteR2dbcProperties,
    private val readR2dbcProperties: DummyReadR2dbcProperties
) : AbstractR2dbcConfiguration() {
    private companion object {
        const val CONNECTION_FACTORY = DUMMY_CONNECTION_FACTORY
        const val TRANSACTION_MANAGER = DUMMY_TRANSACTION_MANAGER
        const val TRANSACTIONAL_OPERATOR = DUMMY_TRANSACTIONAL_OPERATOR
        const val ENTITY_OPERATIONS = DUMMY_ENTITY_OPERATIONS
    }

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Primary
    @Bean(CONNECTION_FACTORY)
    override fun connectionFactory(): ConnectionFactory {
        val factories = HashMap<ConnectionType, ConnectionFactory>()
        factories[WRITE] = writeR2dbcProperties.getConnectionFactory()
        factories[READ] = readR2dbcProperties.getConnectionFactory()

        val multiTenantRoutingConnectionFactory = object : AbstractRoutingConnectionFactory() {
            override fun determineCurrentLookupKey(): Mono<Any> = mono {
                val context = coroutineContext[ReactorContext]?.context
                val connectionType = if (context != null && context.hasKey(REACTOR_CONTEXT_CONNECTION_TYPE)) {
                    context.get<R2dbcConnectionType>(REACTOR_CONTEXT_CONNECTION_TYPE)
                } else {
                    null
                }
                logger.debug("current-thread-name : {}, connectionType : {}", Thread.currentThread().name, connectionType)
                connectionType?.userConnectionType
            }
        }
        multiTenantRoutingConnectionFactory.setDefaultTargetConnectionFactory(writeR2dbcProperties.getConnectionFactory())
        multiTenantRoutingConnectionFactory.setTargetConnectionFactories(factories)
        return multiTenantRoutingConnectionFactory
    }

    @Primary
    @Bean(TRANSACTION_MANAGER)
    fun transactionManager(
        @Qualifier(CONNECTION_FACTORY) connectionFactory: ConnectionFactory
    ) = R2dbcTransactionManager(connectionFactory)

    @Primary
    @Bean(TRANSACTIONAL_OPERATOR)
    fun transactionalOperator(
        @Qualifier(TRANSACTION_MANAGER) transactionManager: R2dbcTransactionManager
    ) = TransactionalOperator.create(transactionManager)

    @Bean(ENTITY_OPERATIONS)
    fun entityOperations(
        @Qualifier(CONNECTION_FACTORY) connectionFactory: ConnectionFactory,
        r2dbcConverter: R2dbcConverter
    ): R2dbcEntityOperations {
        val strategy = DefaultReactiveDataAccessStrategy(MySqlDialect.INSTANCE, r2dbcConverter)
        val databaseClient = DatabaseClient.builder().connectionFactory(connectionFactory).build()
        return R2dbcEntityTemplate(databaseClient, strategy)
    }
}

/**
 * Write R2dbc Properties.
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "datasource.dummy.write")
data class DummyWriteR2dbcProperties(
    override val protocol: String,
    override val host: String,
    override val port: Int,
    override val database: String,
    override val username: String,
    override val password: String,
    override val pooling: PoolingProperties = PoolingProperties()
) : R2dbcProperties(
    protocol, host, port, database, username, password, pooling
)

/** Read R2dbc Properties.
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "datasource.dummy.read")
data class DummyReadR2dbcProperties(
    override val protocol: String,
    override val host: String,
    override val port: Int,
    override val database: String,
    override val username: String,
    override val password: String,
    override val pooling: PoolingProperties = PoolingProperties()
) : R2dbcProperties(
    protocol, host, port, database, username, password, pooling
)
