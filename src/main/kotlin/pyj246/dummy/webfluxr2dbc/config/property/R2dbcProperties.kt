package pyj246.dummy.webfluxr2dbc.config.property

import io.r2dbc.pool.PoolingConnectionFactoryProvider
import io.r2dbc.spi.ConnectionFactories
import io.r2dbc.spi.ConnectionFactoryOptions
import java.time.Duration

/**
 * 기본 R2DBC 프로퍼티 클래스.
 */
open class R2dbcProperties(
    open val protocol: String,
    open val host: String,
    open val port: Int,
    open val database: String,
    open val username: String,
    open val password: String,
    open val pooling: PoolingProperties
) {
    fun getConnectionFactory() = ConnectionFactories.get(
        ConnectionFactoryOptions.builder()
            .option(ConnectionFactoryOptions.DRIVER, PoolingConnectionFactoryProvider.POOLING_DRIVER)
            .option(ConnectionFactoryOptions.PROTOCOL, protocol)
            .option(ConnectionFactoryOptions.HOST, host)
            .option(ConnectionFactoryOptions.PORT, port)
            .option(ConnectionFactoryOptions.DATABASE, database)
            .option(ConnectionFactoryOptions.USER, username)
            .option(ConnectionFactoryOptions.PASSWORD, password)
            .option(PoolingConnectionFactoryProvider.MAX_CREATE_CONNECTION_TIME, Duration.ofSeconds(pooling.maxCreationConnectionSecs))
            .option(PoolingConnectionFactoryProvider.INITIAL_SIZE, pooling.initialSize)
            .option(PoolingConnectionFactoryProvider.MAX_SIZE, pooling.maxSize)
            .option(PoolingConnectionFactoryProvider.MAX_IDLE_TIME, Duration.ofSeconds(pooling.maxIdleTimeSecs))
            .option(PoolingConnectionFactoryProvider.MAX_LIFE_TIME, Duration.ofSeconds(pooling.maxLifeTimeSecs))
            .build()
    )

    data class PoolingProperties(
        val maxCreationConnectionSecs: Long = 5,
        val initialSize: Int = 10,
        val maxSize: Int = 30,
        val maxIdleTimeSecs: Long = 60,
        val maxLifeTimeSecs: Long = 300
    )
}