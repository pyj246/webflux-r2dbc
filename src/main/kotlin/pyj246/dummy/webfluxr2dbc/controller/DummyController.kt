package pyj246.dummy.webfluxr2dbc.controller

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pyj246.dummy.webfluxr2dbc.model.dummy.DummyPostPutRequest
import pyj246.dummy.webfluxr2dbc.model.dummy.DummyResponse
import pyj246.dummy.webfluxr2dbc.service.DummyService

@RequestMapping("/dummies")
@RestController
class DummyController(
    private val dummyService: DummyService
) {

    @GetMapping
    suspend fun getList(): Flow<DummyResponse> = dummyService.getList().map { DummyResponse(it) }

    @GetMapping("/{id}")
    suspend fun get(
        @PathVariable id: Long
    ): DummyResponse {
        val dummyEntity = dummyService.get(id)
        return DummyResponse(dummyEntity)
    }

    @PostMapping
    suspend fun post(
        @RequestBody dummyPostRequest: DummyPostPutRequest
    ): DummyResponse {
        val dummyEntity = dummyService.create(dummyPostRequest.content)
        return DummyResponse(dummyEntity)
    }

    @PutMapping("/{id}")
    suspend fun put(
        @PathVariable id: Long,
        @RequestBody dummyPostRequest: DummyPostPutRequest
    ): DummyResponse {
        val dummyEntity = dummyService.update(id, dummyPostRequest.content)
        return DummyResponse(dummyEntity)
    }

    @DeleteMapping("/{id}")
    suspend fun delete(
        @PathVariable id: Long
    ) = dummyService.delete(id)
}