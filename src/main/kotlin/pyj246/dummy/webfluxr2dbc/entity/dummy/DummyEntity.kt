package pyj246.dummy.webfluxr2dbc.entity.dummy

import java.time.LocalDateTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactor.mono
import org.reactivestreams.Publisher
import org.springframework.context.annotation.Lazy
import org.springframework.data.annotation.Id
import org.springframework.data.r2dbc.mapping.OutboundRow
import org.springframework.data.r2dbc.mapping.event.AfterSaveCallback
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.relational.core.sql.SqlIdentifier
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository

/**
 * 더미 정보 엔티티 데이터 클래스.
 */
@Table("dummy")
data class DummyEntity(
    @Id val id: Long? = null,
    val content: String,
    val deleted: Boolean,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime
) {
    fun toHistory() = DummyHistoryEntity(
        dummyId = id!!,
        content = content,
        deleted = deleted,
        createdAt = updatedAt
    )
}

/**
 * 더미 변경이력 엔티티 데이터 클래스.
 */
@Table("dummy_history")
data class DummyHistoryEntity(
    @Id val id: Long? = null,
    val dummyId: Long,
    val content: String,
    val deleted: Boolean,
    val createdAt: LocalDateTime
)

