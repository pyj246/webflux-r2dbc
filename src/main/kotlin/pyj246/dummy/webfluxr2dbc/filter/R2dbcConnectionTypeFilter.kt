package pyj246.dummy.webfluxr2dbc.filter

import kotlinx.coroutines.reactive.awaitSingleOrNull
import kotlinx.coroutines.reactor.mono
import org.springframework.http.HttpMethod.DELETE
import org.springframework.http.HttpMethod.PATCH
import org.springframework.http.HttpMethod.POST
import org.springframework.http.HttpMethod.PUT
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import pyj246.dummy.webfluxr2dbc.model.r2dbc.ConnectionType.READ
import pyj246.dummy.webfluxr2dbc.model.r2dbc.ConnectionType.WRITE
import pyj246.dummy.webfluxr2dbc.model.r2dbc.R2dbcConnectionType
import pyj246.dummy.webfluxr2dbc.model.r2dbc.R2dbcConnectionType.Companion.REACTOR_CONTEXT_CONNECTION_TYPE
import reactor.core.publisher.Mono

/**
 * R2DBC 커넥션 타입을 결정하는 필터.
 */
@Component
class R2dbcConnectionTypeFilter : WebFilter {
    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> = mono {
        chain.filter(exchange)
            .contextWrite { context ->
                val httpMethod = exchange.request.method
                val userConnectionType = httpMethod?.let { method ->
                    when(method) {
                        POST, PUT, PATCH, DELETE -> WRITE
                        else -> READ
                    }
                } ?: WRITE

                context.put(REACTOR_CONTEXT_CONNECTION_TYPE,
                    R2dbcConnectionType(
                        method = httpMethod,
                        path = exchange.request.path.toString(),
                        userConnectionType = userConnectionType
                    )
                )
            }.awaitSingleOrNull()
    }
}