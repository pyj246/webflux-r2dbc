package pyj246.dummy.webfluxr2dbc.filter

import kotlinx.coroutines.reactive.awaitSingleOrNull
import kotlinx.coroutines.reactor.mono
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

/**
 * 요청 로그를 남기는 웹필터 클래스.
 */
@Component
class RequestLogFilter : WebFilter {
    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> = mono {
        logger.info("requested.(method: {}, path: {})", exchange.request.method, exchange.request.path)
        chain.filter(exchange).awaitSingleOrNull()
    }
}