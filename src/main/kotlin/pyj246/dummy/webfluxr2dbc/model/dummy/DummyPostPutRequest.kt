package pyj246.dummy.webfluxr2dbc.model.dummy

data class DummyPostPutRequest(
    val content: String
)