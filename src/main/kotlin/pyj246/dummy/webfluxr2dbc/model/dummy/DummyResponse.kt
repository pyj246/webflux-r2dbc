package pyj246.dummy.webfluxr2dbc.model.dummy

import java.time.LocalDateTime
import pyj246.dummy.webfluxr2dbc.entity.dummy.DummyEntity

data class DummyResponse(
    val id: Long,
    val content: String,
    val updatedAt: LocalDateTime
) {
    constructor(dummyEntity: DummyEntity) : this(
        id = dummyEntity.id!!,
        content = dummyEntity.content,
        updatedAt = dummyEntity.updatedAt
    )
}
