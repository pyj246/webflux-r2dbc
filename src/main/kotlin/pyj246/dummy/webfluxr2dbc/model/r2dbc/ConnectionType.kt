package pyj246.dummy.webfluxr2dbc.model.r2dbc

enum class ConnectionType {
    WRITE, READ
}

