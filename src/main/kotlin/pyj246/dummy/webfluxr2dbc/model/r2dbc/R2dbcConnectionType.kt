package pyj246.dummy.webfluxr2dbc.model.r2dbc

import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.reactor.ReactorContext
import org.springframework.http.HttpMethod

data class R2dbcConnectionType(
    val method: HttpMethod?,
    val path: String?,
    var userConnectionType: ConnectionType = ConnectionType.WRITE
) {
    companion object {
        const val REACTOR_CONTEXT_CONNECTION_TYPE = "reactorContextConnectionType"
    }
}

fun CoroutineContext.changeUserConnectionType(connectionType: ConnectionType) {
    this[ReactorContext]?.context
    val context = this[ReactorContext]?.context
    if (context != null && context.hasKey(R2dbcConnectionType.REACTOR_CONTEXT_CONNECTION_TYPE)) {
        val requestConnectionType = context.get<R2dbcConnectionType>(R2dbcConnectionType.REACTOR_CONTEXT_CONNECTION_TYPE)
        requestConnectionType.userConnectionType = connectionType
    }
}