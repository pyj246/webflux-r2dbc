package pyj246.dummy.webfluxr2dbc.repository.dummy

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactor.mono
import org.reactivestreams.Publisher
import org.springframework.context.annotation.Lazy
import org.springframework.data.r2dbc.mapping.OutboundRow
import org.springframework.data.r2dbc.mapping.event.AfterSaveCallback
import org.springframework.data.relational.core.sql.SqlIdentifier
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository
import pyj246.dummy.webfluxr2dbc.entity.dummy.DummyEntity
import pyj246.dummy.webfluxr2dbc.entity.dummy.DummyHistoryEntity

/**
 * dummy 리파지토리 인터페이스.
 */
@Repository
interface DummyRepository : CoroutineCrudRepository<DummyEntity, Long> {
    suspend fun findByIdAndDeletedIsFalse(id: Long): DummyEntity?

    fun findAllByDeletedIsFalseOrderByIdDesc(): Flow<DummyEntity>
}

/**
 * dummy_history 리파지토리 인터페이스.
 */
@Repository
interface DummyHistoryRepository : CoroutineCrudRepository<DummyHistoryEntity, Long>

/**
 * dummy 저장후처리 클래스.
 */
@Component
class UsersEntityAfterSaveCallback(
    @Lazy private val dummyHistoryRepository: DummyHistoryRepository,
) : AfterSaveCallback<DummyEntity> {
    override fun onAfterSave(
        entity: DummyEntity,
        outboundRow: OutboundRow,
        table: SqlIdentifier
    ): Publisher<DummyEntity> =
        mono(Dispatchers.IO) {
            dummyHistoryRepository.save(entity.toHistory())
            entity
        }
}


