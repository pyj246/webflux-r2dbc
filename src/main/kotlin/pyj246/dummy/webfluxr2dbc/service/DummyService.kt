package pyj246.dummy.webfluxr2dbc.service

import kotlinx.coroutines.flow.Flow
import org.springframework.stereotype.Service
import pyj246.dummy.webfluxr2dbc.entity.dummy.DummyEntity
import pyj246.dummy.webfluxr2dbc.repository.dummy.DummyRepository
import pyj246.dummy.webfluxr2dbc.utils.DatetimeUtils.localDateTimeNow

@Service
class DummyService(
    private val dummyRepository: DummyRepository
) {

    fun getList(): Flow<DummyEntity> = dummyRepository.findAllByDeletedIsFalseOrderByIdDesc()

    suspend fun get(id: Long): DummyEntity = dummyRepository.findByIdAndDeletedIsFalse(id)
        ?: throw RuntimeException("not exists dummy.(id: $id)")

    suspend fun create(content: String): DummyEntity {
        val now = localDateTimeNow()
        return dummyRepository.save(
            DummyEntity(
                content = content,
                deleted = false,
                createdAt = now,
                updatedAt = now
            )
        )
    }

    suspend fun update(id: Long, content: String): DummyEntity {
        val dummyEntity = get(id)
        return dummyRepository.save(
            dummyEntity.copy(
                content = content,
                updatedAt = localDateTimeNow()
            )
        )
    }

    suspend fun delete(id: Long) {
        val dummyEntity = get(id)
        dummyRepository.save(
            dummyEntity.copy(
                deleted = true,
                updatedAt = localDateTimeNow()
            )
        )
    }
}