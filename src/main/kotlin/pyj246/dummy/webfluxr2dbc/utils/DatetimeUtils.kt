package pyj246.dummy.webfluxr2dbc.utils

import java.time.LocalDateTime
import java.time.ZoneOffset

object DatetimeUtils {
    fun localDateTimeNow(): LocalDateTime = LocalDateTime.now(ZoneOffset.UTC)
}