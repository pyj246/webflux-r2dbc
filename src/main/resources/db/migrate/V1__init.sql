-- 사용자 정보
CREATE TABLE dummy (
    id bigserial PRIMARY KEY,
    content varchar(200) NOT NULL,
    deleted boolean NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
COMMENT ON TABLE dummy IS '더미 정보';
COMMENT ON COLUMN dummy.id IS '식별자';
COMMENT ON COLUMN dummy.content IS '내용';
COMMENT ON COLUMN dummy.deleted IS '이메일 인증여부';
COMMENT ON COLUMN dummy.created_at IS '최초 생성일시';
COMMENT ON COLUMN dummy.updated_at IS '최종 수정일시';

CREATE TABLE dummy_history (
    id bigserial PRIMARY KEY,
    dummy_id bigint NOT NULL,
    content varchar(200),
    deleted boolean NOT NULL,
    created_at timestamp NOT NULL
);
COMMENT ON TABLE dummy_history IS '더미 변경이력';
COMMENT ON COLUMN dummy_history.id IS '히스토리 식별자';
COMMENT ON COLUMN dummy_history.dummy_id IS '원본 식별자';
COMMENT ON COLUMN dummy_history.content IS '원본 식별자';
COMMENT ON COLUMN dummy_history.deleted IS '이메일 인증여부';
COMMENT ON COLUMN dummy_history.created_at IS '이력 생성일시(원본 수정일시)';
ALTER TABLE dummy_history ADD CONSTRAINT dummy_history_fx_dummy_id FOREIGN KEY (dummy_id) REFERENCES dummy (id);